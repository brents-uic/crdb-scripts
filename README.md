Okay, `crdb_scripts` is actually a bit of a misnomer, since the repo
contains scripts for both CockroachDB *and* Distbench. Each directory
contains separate READMEs that describe how to use the scripts included
in the repo.
