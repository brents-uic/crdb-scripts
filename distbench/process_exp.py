import argparse
from cycler import cycler
import datetime
import glob
import matplotlib
import matplotlib.pyplot as plt
import os
import re
import yaml
from pprint import pprint
from os.path import isfile, join
from prototxt_parser.prototxt import parse
from processing import process_timeseries
import multiprocessing as mp

def get_peer_timestamps(fname):

    with open(fname) as f:
        s = f.read()

    try:
        d = parse(s)
    except:
        print("FAILED TO PARSE", fname)
        return None, None

    qps = None
    for attr in d["test_results"]["traffic_config"]["attributes"]:
        if attr["key"] == "qps":
            qps = attr["value"]


    instance_logs = d["test_results"]["service_logs"]["instance_logs"]
    n_senders = len(instance_logs)

    results = {}

    for i in range(n_senders):
        sender_name = instance_logs[i]["key"]
        peer_logs = instance_logs[i]["value"]["peer_logs"]
        results[sender_name] = {}

        n_peers = len(peer_logs)
        for j in range(n_peers):
            peer_name = instance_logs[i]["value"]["peer_logs"][j]["key"]
            timestamps =  instance_logs[i]["value"]["peer_logs"][j]["value"]["rpc_logs"]["value"]["successful_rpc_samples"]
            results[sender_name][peer_name] = []

            for d in timestamps:
                start = d["start_timestamp_ns"]
                latency = d["latency_ns"]
                results[sender_name][peer_name].append([start, start + latency])

    print("Done parsing", fname)
    return qps, results


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("rx", nargs="+", help="Regular expression to match files on")

    args = parser.parse_args()

    # Go through the entire list of files. Ignore anything that doesn't end
    # in .pb or .config. Every .pb file must have a .config file that
    # corresponds to it, so we'll use those to get the qps number. We are ONLY
    # graphing against qps, so no need to make this "flexible".
    exp_data = []
    result_queue = []

    files = [f for f in args.rx if f[-3:] == ".pb"]
    print(files)
    with mp.Pool(len(files)) as pool:
        result_queue = pool.map(get_peer_timestamps, args.rx)

    print(len(result_queue))

    for qps, results in result_queue:
        qps = int(qps)
        timestamps = []

        # Aggregate timestamps
        for receiver in results["client/0"].keys():
            timestamps.extend(results["client/0"][receiver])

        for receiver in results["client/1"].keys():
            timestamps.extend(results["client/0"][receiver])

        timestamps.sort(key=lambda x: x[0])

        # NOTE: You need to be explicit about the duration of your experiment
        # here. This should be turned into a CLI flag as well, or simply parsed
        # from the PB file.
        _, agg_data = process_timeseries(timestamps, duration=30)

        agg_data["qps"] = qps
        exp_data.append(agg_data)

    # TODO: Make the output file name a CLI flag
    with open("processed_data.yaml", "w") as f:
        yaml.dump(exp_data, f)


if __name__ == "__main__":
    main()
