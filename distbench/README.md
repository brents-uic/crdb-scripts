This is a "collection" of scripts that I used when plotting
Distbench experiments. *These are hacky, unstable, and sometimes
require modifying the source code in order to work with particular
experimental parameters.* 

With that being said, however, these scripts should be capable of taking
plain-text .pb files (I know, I know) parsing them, and extracting data on
throughput, latency, and the number of outstanding requests. Everything is
measured in 100ms windows for each experiment, and the duration of the
experiment must be known beforehand. You'll need to modify the value in
`process_exp.py`.

Spinning up Distbench in CloudLab should be a known process, but I will
add instructions on how to do it at a later time.

This should only have a couple dependencies:
* pyyaml
* prototxt-parser
* matplotlib

`pip3 install -r requirements.txt` should install everything you need to
run the scripts.
