# This is a one-off script for plotting the output files of
# process_exp.py. You should use your own script to parse the
# YAML files and plot them instead of this one.

import matplotlib.pyplot as plt
import yaml
from pprint import pprint
from cycler import cycler

plt.rcParams["axes.prop_cycle"] = cycler(color=[
    "#000000", "#CD0000", "#00CD00", "#0000EE", "#CD00CD", "#00CDCD", "#7F7F7F", "#75507B"])


with open("processed_data.yaml") as f:
    d = yaml.full_load(f)

d.sort(key = lambda x: x["qps"])

qps = []
throughput = {
    "p50": [],
    "p10": [],
    "p1": []
}

latency = {
    "p99": [],
    "p90": [],
    "p50": []
}

for data in d:

    qps.append(data["qps"])
    for p, n in data["latency"].items():
        latency[p].append(n)

    for p, n in data["throughput"].items():
        throughput[p].append(n)

plt.figure(figsize=(2, 1.75))

qps = [q/1000 for q in qps]

for p, t in latency.items():
    plt.plot(qps, [i for i in t], label=p)

print(latency)
plt.yscale("log")
plt.legend(fontsize="xx-small", loc="upper left")
plt.yticks([1, 10, 100, 1000, 10000])
plt.xticks([0, 20, 40, 60, 80])
plt.grid()
plt.xlabel("Target Offered Load \n(Krps)")
plt.ylabel("Latency (ms)")
plt.tight_layout()
plt.savefig("plot.png")
